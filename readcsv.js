import { WebClient } from '@slack/web-api';
import { token_user_slack, ChanelId } from "../data";
import fs from 'fs';
import axios from 'axios';
import csv from 'csv-parser';


// Slack API token and channel ID
const token = token_user_slack;
const channelIds = ChanelId;
const web = new WebClient(token);

// Function to download the file from Slack
async function downloadFile(fileUrl, destinationPath) {
  try {
    const response = await axios.get(fileUrl, {
      headers: { Authorization: `Bearer ${token}` },
      responseType: 'stream',
    });

    const writer = fs.createWriteStream(destinationPath);
    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on('finish', () => resolve(destinationPath));
      writer.on('error', reject);
    });
  } catch (error) {
    console.error('Error downloading file:', error);
    throw error;
  }
}

// Function to read the CSV file
async function readCsvFile(filePath) {
  return new Promise((resolve, reject) => {
    const results = [];
    fs.createReadStream(filePath)
      .pipe(csv())
      .on('data', (data) => results.push(data))
      .on('end', () => resolve(results))
      .on('error', (error) => reject(error));
  });
}

// Function to get the file URL from a message
async function getFileUrlFromMessage() {
  try {
    const result = await web.conversations.history({
      channel: channelIds,
      limit: 10,
    });

    for (const message of result.messages) {
      if (message.files) {
        for (const file of message.files) {
          if (file.mimetype === 'text/csv') {
            console.log('File URL:', file.url_private_download);
            return file.url_private_download;
          }
        }
      }
    }

    throw new Error('No CSV file found in recent messages');
  } catch (error) {
    console.error('Error fetching message history:', error.response ? error.response.data : error.message);
    throw error;
  }
}

// Function to send a message to Slack
async function sendMessage(channel, message, ts, type) {
  try {
    const reaction = type === 'wrong' ? 'x' : 'white_check_mark';
    await web.reactions.add({
      channel,
      timestamp: ts,
      name: reaction
    });
    await web.chat.postMessage({
      channel: channel,
      thread_ts: ts,
      text: message,
    });
  } catch (err) {
    console.error('Error in sendMessage function:', err);
  }
}

// Main function to download and read the CSV file from an attachment
async function main(channelId, userId, ts) {

  try {
    const fileUrl = await getFileUrlFromMessage();
    console.log('File URL:', fileUrl);
    const downloadedFilePath = 'downloaded_file.csv';
    await downloadFile(fileUrl, downloadedFilePath);
    const csvData = await readCsvFile(downloadedFilePath);
    
    for (const row of csvData) {
      await createAndUpdateVoucher(
        row.nama_voucher, // voucher_code
        row.nominal, // amount
        row.total_voucher, // init_quota
        row.tgl_mulai, // startAtInput
        row.tgl_akhir, // endAtInput
        row.alokasi, // allocation
        channelId, 
        userId, 
        ts, 
        sendMessage
      );
      console.log(`Voucher Code: ${row.nama_voucher}`);
    }
    console.log(csvData);
    
  } catch (error) {
    console.error('Error in main function:', error);
  }
}

export default async function(channelId, userId, ts) {
  await main(channelId, userId, ts);
}
